<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MemberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MemberTable Test Case
 */
class MemberTableTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Member') ? [] : ['className' => 'App\Model\Table\MemberTable'];
        $this->Member = TableRegistry::get('Member', $config);
    }

    public function tearDown()
    {
        unset($this->Member);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $member = $this->Member->get(1);

        $this->assertNotNull($member);
        $this->assertEquals(1, $member->id);
    }

    public function testDateJoinedConversionValid()
    {
        $member = $this->Member->get(1);
        $member = $this->Member->patchEntity($member, ['dateJoined' => '2015-12-01T12:00Z']);

        $this->assertInstanceOf('DateTime', $member['dateJoined']);
    }

    public function testDateJoinedConversionInValid()
    {
        $member = $this->Member->get(1);
        $data = ['dateJoined' => '2015-13-01T12:00Z'];

        $member = $this->Member->patchEntity($member, $data);

        $this->assertNotNull($member->getErrors()['dateJoined']);
    }

    public function testDateLeftConversionValid()
    {
        $member = $this->Member->get(1);
        $member = $this->Member->patchEntity($member, ['dateLeft' => '2015-12-01T12:00Z']);

        $this->assertInstanceOf('DateTime', $member['dateLeft']);
    }

    public function testDateLeftConversionInValid()
    {
        $member = $this->Member->get(1);
        $data = ['dateLeft' => '2015-13-01T12:00Z'];

        $member = $this->Member->patchEntity($member, $data);

        $this->assertNotNull($member->getErrors()['dateLeft']);
    }

    public function testEmailValid()
    {
        $member = $this->Member->get(1);
        $this->Member->patchEntity($member, ['email' => 'valid@email.com']);

        $this->assertCount(0, $member->getErrors());
    }

    public function testEmailInvalid()
    {
        $member = $this->Member->get(1);
        $member = $this->Member->patchEntity($member, ['email' => 'invalid.email.example.com']);

        $this->assertNotNull($member->getErrors()['email']);
    }
}
