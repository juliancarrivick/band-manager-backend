<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MemberInstrumentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MemberInstrumentTable Test Case
 */
class MemberInstrumentTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.MemberInstrument'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MemberInstrument') ? [] : ['className' => 'App\Model\Table\MemberInstrumentTable'];
        $this->MemberInstrument = TableRegistry::get('MemberInstrument', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MemberInstrument);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $instrument = $this->MemberInstrument->get(1);

        $this->assertNotNull($instrument);
        $this->assertEquals(1, $instrument->id);
    }
}
