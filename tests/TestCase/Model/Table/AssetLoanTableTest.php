<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssetLoanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class AssetLoanTableTest extends TestCase
{
    public $AssetLoan;

    public $fixtures = [
        'app.Users',
        'app.Asset',
        'app.Member',
        'app.AssetLoan'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AssetLoan') ? [] : ['className' => 'App\Model\Table\AssetLoanTable'];
        $this->AssetLoan = TableRegistry::get('AssetLoan', $config);
    }

    public function tearDown()
    {
        unset($this->AssetLoan);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $loan = $this->AssetLoan->get(1);

        $this->assertNotNull($loan);
        $this->assertEquals(1, $loan->id);
    }

    public function testDateBorrowedConversionValid()
    {
        $loan = $this->AssetLoan->get(1);
        $data = ['dateBorrowed' => '2015-12-01T12:00Z'];

        $loan = $this->AssetLoan->patchEntity($loan, $data);

        $this->assertInstanceOf('DateTime', $loan->dateBorrowed);
    }

    public function testDateBorrowedConversionInValid()
    {
        $loan = $this->AssetLoan->get(1);
        $data = ['dateBorrowed' => '2015-13-01T12:00Z'];

        $this->AssetLoan->patchEntity($loan, $data);

        $this->assertNotEmpty($loan->getErrors()['dateBorrowed']);
    }

    public function testDateReturnedConversionValid()
    {
        $loan = $this->AssetLoan->get(1);
        $data = ['dateReturned' => '2015-12-01T12:00Z'];

        $loan = $this->AssetLoan->patchEntity($loan, $data);

        $this->assertInstanceOf('DateTime', $loan->dateReturned);
    }

    public function testDateReturnedConversionInValid()
    {
        $loan = $this->AssetLoan->get(1);
        $data = ['dateReturned' => '2015-13-01T12:00Z'];

        $this->AssetLoan->patchEntity($loan, $data);

        $this->assertNotEmpty($loan->getErrors()['dateReturned']);
    }
}
