<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PerformanceScoreTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class PerformanceScoreTableTest extends TestCase
{
    public $PerformanceScore;

    public $fixtures = [
        'app.Ensemble',
        'app.Concert',
        'app.Performance',
        'app.Score',
        'app.PerformanceScore'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PerformanceScore') ? [] : ['className' => 'App\Model\Table\PerformanceScoreTable'];
        $this->PerformanceScore = TableRegistry::get('PerformanceScore', $config);
    }

    public function tearDown()
    {
        unset($this->PerformanceScore);
        parent::tearDown();
    }

    public function testInitialize()
    {
        $performanceScore = $this->PerformanceScore->get(1);
        $this->assertEquals(1, $performanceScore->id);
    }
}
