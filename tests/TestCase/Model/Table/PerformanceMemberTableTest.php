<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PerformanceMemberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class PerformanceMemberTableTest extends TestCase
{
    public $PerformanceMember;

    public $fixtures = [
        'app.Users',
        'app.Ensemble',
        'app.Concert',
        'app.Performance',
        'app.Member',
        'app.PerformanceMember'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PerformanceMember') ? [] : ['className' => 'App\Model\Table\PerformanceMemberTable'];
        $this->PerformanceMember = TableRegistry::get('PerformanceMember', $config);
    }

    public function tearDown()
    {
        unset($this->PerformanceMember);
        parent::tearDown();
    }

    public function testInitialize()
    {
        $performanceMember = $this->PerformanceMember->get(1);
        $this->assertNotNull($performanceMember);
        $this->assertEquals(1, $performanceMember->id);
    }
}
