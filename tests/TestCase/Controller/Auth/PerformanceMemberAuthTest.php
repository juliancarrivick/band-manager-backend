<?php
namespace App\Test\TestCase\Controller\Auth;

class PerformanceMemberAuthTest extends AuthTestCase {
    public function testGetMembers() {
        $endpoint = '/performance/1/member';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPostPerformanceMember() {
        $endpoint = '/performance/1/member/1';
        $this->assertStandardPostAccessForEndpoint($endpoint, []);
    }

    public function testDeletePerformanceMember() {
        $endpoint = '/performance/1/member/1';
        $this->assertStandardDeleteAccessForEndpoint($endpoint);
    }
}