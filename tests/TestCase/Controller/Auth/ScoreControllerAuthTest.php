<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\ScoreControllerAuth;
use App\Test\TestCase\DomainObjectFactory;

class ScoreControllerAuthTest extends AuthTestCase {

    public function testIndex() {
        $endpoint = '/score';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testView() {
        $endpoint = '/score/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testAdd() {
        $newScore = DomainObjectFactory::createScoreMap();
        unset($newScore['id']);
        $newScore['title'] = 'Fur Elise';

        $endpoint = '/score';
        $this->assertStandardPostAccessForEndpoint($endpoint, $newScore);
    }

    public function testEditAdmin() {
        $score = ['title' => 'Fur Elise'];

        $endpoint = '/score/1';
        $this->assertStandardPutAccessForEndpoint($endpoint, $score);
    }

    public function testDeleteAdmin() {
        $mockScore = DomainObjectFactory::createScoreMap();
        $recreateCallback = $this->generateNewObjectFunctionWithId('Score', $mockScore, 1);

        $endpoint = '/score/1';
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateCallback);
    }
}
