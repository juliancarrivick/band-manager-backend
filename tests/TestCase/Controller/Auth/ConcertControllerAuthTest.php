<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\ConcertController;
use App\Test\TestCase\DomainObjectFactory;

class ConcertControllerAuthTest extends AuthTestCase {
    public function testIndex() {
        $endpoint = '/concert';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testViewdmin() {
        $endpoint = '/concert/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testAdd() {
        $endpoint = '/concert';

        $newConcert = DomainObjectFactory::createConcertMap();
        unset($newConcert['id']);
        $newConcert['occasion'] = 'State Championships';

        $this->assertStandardPostAccessForEndpoint('/concert', $newConcert);
    }

    public function testEdit() {
        $endpoint = '/concert/1';
        $modifiedConcert = [
            'occasion' => 'State Championships'
        ];

        $this->assertStandardPutAccessForEndpoint('/concert/1', $modifiedConcert);
    }

    public function testDeleteAdmin() {
        $endpoint = '/concert/1';
        $mockConcert = DomainObjectFactory::createConcertMap();
        $recreateConcert = $this->generateNewObjectFunctionWithId('Concert', $mockConcert, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateConcert);
    }

    public function testGetConcertsByMember() {
        $endpoint = '/member/1/concert';
        $this->assertStandardGetAccessForEndpoint($endpoint, 'user');
    }
}
