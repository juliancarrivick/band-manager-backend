<?php
namespace App\Test\TestCase\Controller\Auth;

class RoleControllerAuthTest extends AuthTestCase
{
    private $authorisedUsers = ['admin'];
    private $deniedUsers = ['editor', 'viewer', 'unknown'];

    public function testIndex()
    {
        $endpoint = '/role';
        $this->assertAdminOnlyGetAccessForEndpoint($endpoint);
    }

    public function testByUserId()
    {
        $endpoint = '/users/1/role';
        $this->assertAdminOnlyGetAccessForEndpoint($endpoint);
    }

    public function testAddUserToRole()
    {
        $endpoint = '/users/1/role/1';
        $this->assertPostAccessForEndpoint($endpoint, [], $this->authorisedUsers, $this->deniedUsers);
    }

    public function testRemoveUserFromRole()
    {
        $endpoint = '/users/1/role/1';
        $callback = function () {}; // Empty to satisfy interface - this will need future refactoring
        $this->assertDeleteAccessForEndpoint($endpoint, $callback, $this->authorisedUsers, $this->deniedUsers);
    }

    public function testByEnsembleId()
    {
        $endpoint = '/ensemble/1/role';
        $this->assertAdminOnlyGetAccessForEndpoint($endpoint);
    }

    public function testAddEnsembleToRole()
    {
        $endpoint = '/ensemble/1/role/1';
        $this->assertPostAccessForEndpoint($endpoint, [], $this->authorisedUsers, $this->deniedUsers);
    }

    public function testRemoveEnsembleFromRole()
    {
        $endpoint = '/ensemble/1/role/1';
        $callback = function () {}; // Empty to satisfy interface - this will need future refactoring
        $this->assertDeleteAccessForEndpoint($endpoint, $callback, $this->authorisedUsers, $this->deniedUsers);
    }
}
