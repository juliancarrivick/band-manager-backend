<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Test\TestCase\DomainObjectFactory;

class MemberSummaryControllerAuthTest extends AuthTestCase {
    public function testAllMemberSummary() {
        $endpoint = '/member/summary';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testCurrentMemberSummary() {
        $endpoint = '/member/current/summary';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }
}
