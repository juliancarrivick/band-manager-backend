<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Test\TestCase\DomainObjectFactory;

class MemberControllerAuthTest extends AuthTestCase
{
    public function testIndex()
    {
        $endpoint = '/member';
        $this->assertAdminAndEditorOnlyGetAccessForEndpoint($endpoint);
    }

    public function testView()
    {
        $endpoint = '/member/1';
        $this->assertAdminAndEditorOnlyGetAccessForEndpoint($endpoint);
    }

    public function testAddAdmin()
    {
        $endpoint = '/member';

        $newMember = DomainObjectFactory::createMemberMap();
        unset($newMember['id']);
        $newMember['description'] = 'Bass Clarinet';

        $this->assertStandardPostAccessForEndpoint($endpoint, $newMember, function () use ($newMember) {
            $memberTable = $this->getTable('member');
            $member = $memberTable->find()
                ->where(['email' => $newMember['email']])
                ->first();
            $memberTable->delete($member);
        });
    }

    public function testEditAdmin()
    {
        $endpoint = '/member/1';
        $modifiedMember = ['description' => 'Bass Clarinet'];

        $this->assertStandardPutAccessForEndpoint($endpoint, $modifiedMember);
    }

    public function testDeleteAdmin()
    {
        $endpoint = '/member/1';
        $mockMember = DomainObjectFactory::createMemberMap();
        $recreateMemberCallback = $this->generateNewObjectFunctionWithId('Member', $mockMember, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateMemberCallback);
    }
}
