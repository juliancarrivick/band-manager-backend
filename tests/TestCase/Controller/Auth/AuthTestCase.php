<?php

namespace App\Test\TestCase\Controller\Auth;

use App\Test\TestCase\DomainObjectFactory;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

abstract class AuthTestCase extends TestCase
{
    use IntegrationTestTrait;

    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.MemberInstrument',
        'app.Asset',
        'app.AssetLoan',
        'app.Ensemble',
        'app.EnsembleMembership',
        'app.Concert',
        'app.Performance',
        'app.Score',
        'app.PerformanceScore',
        'app.PerformanceMember',
        'app.Role',
        'app.UserRole',
        'app.EnsembleRole',
    ];

    public function generateNewObjectFunction($tableName, $rowMap)
    {
        return $this->generateNewObjectFunctionWithId($tableName, $rowMap, 0);
    }

    public function generateNewObjectFunctionWithId($tableName, $rowMap, $newId)
    {
        return function () use ($tableName, $rowMap, $newId) {
            $table = $this->getTable($tableName);
            $newRow = $table->newEntity($rowMap);
            $newRow->id = $newId;
            $table->save($newRow);
        };
    }

    public function getTable($table)
    {
        return TableRegistry::get($table);
    }

    public function assertStandardGetAccessForEndpoint($endpoint)
    {
        $granted = ['admin', 'editor', 'viewer'];
        $denied = ['unknown'];
        $this->assertGetAccessForEndpoint($endpoint, $granted, $denied);
    }

    public function assertAdminAndEditorOnlyGetAccessForEndpoint($endpoint)
    {
        $granted = ['admin', 'editor'];
        $denied = ['viewer', 'unknown'];
        $this->assertGetAccessForEndpoint($endpoint, $granted, $denied);
    }

    public function assertAdminOnlyGetAccessForEndpoint($endpoint)
    {
        $granted = ['admin'];
        $denied = ['editor', 'viewer', 'unknown'];
        $this->assertGetAccessForEndpoint($endpoint, $granted, $denied);
    }

    public function assertGetAccessForEndpoint($endpoint, array $granted = [], array $denied = [])
    {
        foreach ($granted as $role) {
            $this->assertGetOkAsRole($endpoint, $role);
        }

        foreach ($denied as $role) {
            $this->assertGetUnauthorizedAsRole($endpoint, $role);
        }
    }

    public function assertStandardPostAccessForEndpoint($endpoint, array $body = [], $postOkCallback = null)
    {
        $granted = ['admin', 'editor'];
        $denied = ['viewer', 'unknown'];
        $this->assertPostAccessForEndpoint($endpoint, $body, $granted, $denied, $postOkCallback);
    }

    public function assertPostAccessForEndpoint($endpoint, array $body = [], array $granted = [], array $denied = [], $postOkCallback = null)
    {
        // Create a dummy function if a callback is not passed
        if (!$postOkCallback) {
            $postOkCallback = function () {
            };
        }

        foreach ($granted as $role) {
            $this->assertPostOkAsRole($endpoint, $body, $role);
            $postOkCallback();
        }

        foreach ($denied as $role) {
            $this->assertPostUnauthorizedAsRole($endpoint, $body, $role);
        }
    }

    public function assertStandardPutAccessForEndpoint($endpoint, array $body = [])
    {
        $granted = ['admin', 'editor'];
        $denied = ['viewer', 'unknown'];
        $this->assertPutAccessForEndpoint($endpoint, $body, $granted, $denied);
    }

    public function assertPutAccessForEndpoint($endpoint, array $body = [], array $granted = [], array $denied = [])
    {
        foreach ($granted as $role) {
            $this->assertPutOkAsRole($endpoint, $body, $role);
        }

        foreach ($denied as $role) {
            $this->assertPutUnauthorizedAsRole($endpoint, $body, $role);
        }
    }

    public function assertStandardDeleteAccessForEndpoint($endpoint, $recreateCallback = null)
    {
        $granted = ['admin', 'editor'];
        $denied = ['viewer', 'unknown'];
        $this->assertDeleteAccessForEndpoint($endpoint, $recreateCallback, $granted, $denied);
    }

    public function assertDeleteAccessForEndpoint($endpoint, $recreateCallback = null, array $granted = [], array $denied = [])
    {
        // Create a dummy function if a callback is not passed
        if (!$recreateCallback) {
            $recreateCallback = function () {
            };
        }

        foreach ($granted as $role) {
            $this->assertDeleteOkAsRole($endpoint, $role);
            $recreateCallback();
        }

        foreach ($denied as $role) {
            $this->assertDeleteUnauthorizedAsRole($endpoint, $role);
        }
    }

    public function assertGetOkAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->get($endpoint);
        $this->assertResponseOk();
    }

    public function assertGetUnauthorizedAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->get($endpoint);
        $this->assertResponseCode(302);
    }

    public function assertGetUnauthenticatedAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->get($endpoint);
        $this->assertResponseCode(302);
    }

    public function assertPostOkAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->post($endpoint, $data);
        $this->assertResponseOk();
    }

    public function assertPostUnauthorizedAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->post($endpoint, $data);
        $this->assertResponseCode(302);
    }

    public function assertPostUnauthenticatedAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->post($endpoint, $data);
        $this->assertResponseCode(302);
    }

    public function assertPutOkAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->put($endpoint, $data);
        $this->assertResponseOk();
    }

    public function assertPutUnauthorizedAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->put($endpoint, $data);
        $this->assertResponseCode(302);
    }

    public function assertPutUnauthenticatedAsRole($endpoint, $data, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->put($endpoint, $data);
        $this->assertResponseCode(302);
    }

    public function assertDeleteOkAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->delete($endpoint);
        $this->assertResponseOk();
    }

    public function assertDeleteUnauthorizedAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->delete($endpoint);
        $this->assertResponseCode(302);
    }

    public function assertDeleteUnauthenticatedAsRole($endpoint, $role)
    {
        $this->setUpRequestWithRole($role);
        $this->delete($endpoint);
        $this->assertResponseCode(302);
    }

    private function setUpRequestWithRole($role)
    {
        $this->session([
            'Auth' => ['User' => $this->getUserFromRole($role)],
        ]);
        $this->configRequest([
            'headers' => ['Accept' => 'application/json'],
        ]);
        $this->enableCsrfToken();
    }

    private function getUserFromRole($role)
    {
        $user = DomainObjectFactory::createUserMap();
        $user['roles'] = [
            ['id' => 1, 'role' => $role],
        ];

        return $user;
    }
}
