<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PerformanceScoreController;
use App\Controller\Component\PerformanceScoreServiceComponent;
use Cake\Controller\ComponentRegistry;

class PerformanceScoreControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->PerformanceScoreService = new PerformanceScoreServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->PerformanceScoreService);
        parent::tearDown();
    }

    public function testIndex() {
        $response = $this->get('/performance/1/score');
        $this->assertCount(2, $response->score);
    }

    public function testIndexNonExistant() {
        $this->get('/performance/0/score');
        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $this->post('/performance/1/score/3');
        $this->assertResponseOk();
        $this->assertNumScoresInPerformance(3, 1);
    }

    public function testAddAlreadyExists() {
        $this->post('/performance/1/score/1');
        $this->assertResponseOk();
        $this->assertNumScoresInPerformance(2, 1);
    }

    private function assertNumScoresInPerformance($numScores, $performanceId) {
        $scores = $this->PerformanceScoreService->findByPerformanceId($performanceId);
        $this->assertCount($numScores, $scores);
    }

    public function testAddToUnknownPerformance() {
        $this->get('/performance/0/score/1');
        $this->assertResponseCode(404);
    }

    public function testAddToUnknownScore() {
        $this->get('/performance/1/score/0');
        $this->assertResponseCode(404);
    }

    public function testDelete() {
        $this->delete('/performance/1/score/1');
        $this->assertResponseOk();
        $this->assertNumScoresInPerformance(1, 1);
    }

    public function testDeleteAlreadyDeleted() {
        $this->delete('/performance/1/score/3');
        $this->assertResponseOk();
        $this->assertNumScoresInPerformance(2, 1);
    }

    public function testDeleteUnknownPerformance() {
        $this->delete('performance/0/score/1');
        $this->assertResponseCode(404);
    }

    public function testDeleteUnknownMember() {
        $this->delete('performance/1/score/0');
        $this->assertResponseCode(404);
    }
}
