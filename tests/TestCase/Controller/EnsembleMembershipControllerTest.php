<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EnsembleMembershipController;
use App\Test\TestCase\Controller\RestTestCase;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\EnsembleMembershipServiceComponent;
use \DateTime;

class EnsembleMembershipControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->MembershipService = new EnsembleMembershipServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->MembershipService);

        parent::tearDown();
    }

    public function testMembershipHistory() {
        $responseData = $this->get('/member/1/ensemble_membership');

        $this->assertResponseOk();
        $this->assertResponseContains('membershipHistory');
        $this->assertCount(2, $responseData->membershipHistory);
    }

    public function testIndex() {
        $responseData = $this->get('/ensemble/1/ensemble_membership');

        $this->assertResponseOk();
        $this->assertResponseContains('members');
        $this->assertCount(2, $responseData->members);
    }

    public function testCurrent() {
        $responseData = $this->get('/ensemble/1/ensemble_membership/current');

        $this->assertResponseOk();
        $this->assertResponseContains('members');
        $this->assertCount(1, $responseData->members);
    }

    public function testPastMembers() {
        $responseData = $this->get('/ensemble/1/ensemble_membership/past');

        $this->assertResponseOk();
        $this->assertResponseContains('members');
        $this->assertCount(1, $responseData->members);
    }

    public function testJoin() {
        $responseData = $this->post('/ensemble/1/ensemble_membership/3/join');

        $this->assertResponseOk();
        $this->assertNotNull($responseData->membership);
        $this->assertGreaterThan(0, $responseData->membership->id);
        $this->assertCurrentNumMembersInEnsemble(2, 1);
    }

    public function testJoinAlreadyInEnsemble() {
        $responseData = $this->post('/ensemble/1/ensemble_membership/1/join');

        $this->assertResponseOk();
        $this->assertNotNull($responseData->membership);
        $this->assertGreaterThan(0, $responseData->membership->id);
        $this->assertCurrentNumMembersInEnsemble(1, 1);
    }

    public function testJoinNoEnsemble() {
        $this->delete('/ensemble/0/ensemble_membership/1/join');
        $this->assertResponseCode(404);
    }

    public function testJoinNoMember() {
        $this->delete('/ensemble/1/ensemble_membership/0/join');
        $this->assertResponseCode(404);
    }

    public function testLeave() {
        $this->post('/ensemble/1/ensemble_membership/1/leave');

        $this->assertResponseOk();
        $this->assertCurrentNumMembersInEnsemble(0, 1);
    }

    public function testLeaveNotInEnsemble() {
        $this->post('/ensemble/1/ensemble_membership/2/leave');

        $this->assertResponseOk();
        $this->assertCurrentNumMembersInEnsemble(1, 1);
    }

    public function testLeaveNoEnsemble() {
        $this->delete('/ensemble/0/ensemble_membership/1/leave');
        $this->assertResponseCode(404);
    }

    public function testLeaveNoMember() {
        $this->delete('/ensemble/1/ensemble_membership/0/leave');
        $this->assertResponseCode(404);
    }

    public function testView() {
        $responseData = $this->get('/ensemble_membership/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->membership->id);
    }

    public function testViewNotFound() {
        $responseData = $this->get('/ensemble_membership/0');

        $this->assertResponseCode(404);
    }

    public function testUpdateDateJoined() {
        $updateMap = ['dateJoined' => new DateTime()];

        $oldDateJoined = $this->MembershipService->findById(1)->dateJoined;
        $membership = $this->put('/ensemble_membership/1', $updateMap)->membership;
        $this->assertResponseOk();
        $this->assertNotNull($membership);
        $this->assertNotEquals($oldDateJoined, new DateTime($membership->dateJoined));
    }

    public function testUpdateDateReturned() {
        $updateMap = ['dateLeft' => new DateTime()];

        $oldDateLeft = $this->MembershipService->findById(1)->dateLeft;

        $membership = $this->put('/ensemble_membership/1', $updateMap)->membership;
        $this->assertResponseOk();
        $this->assertNotNull($membership);
        $this->assertNotEquals($oldDateLeft, new DateTime($membership->dateLeft));
    }

    public function testUpdateCantUpdateEnsembleOrMember() {
        $updateMap = [
            'ensembleId' => 3,
            'memberId' => 3
        ];

        $membership = $this->put('/ensemble_membership/1', $updateMap)->membership;
        $this->assertResponseOk();
        $this->assertNotNull($membership);
        $this->assertNotEquals(3, $membership->ensembleId);
        $this->assertNotEquals(3, $membership->memberId);
    }

    public function testDelete() {
        $this->delete('/ensemble_membership/1');

        $this->assertResponseOk();
        $this->assertCurrentNumMembersInEnsemble(0, 1);
    }

    public function testDeleteNotExists() {
        $this->delete('/ensemble_membership/0');

        $this->assertResponseCode(404);
    }

    private function assertCurrentNumMembersInEnsemble($numMembers, $ensembleId) {
        $members = $this->MembershipService->getCurrentMembersByEnsembleId($ensembleId);
        $this->assertCount($numMembers, $members);
    }
}
