<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MemberSummaryController;
use App\Test\TestCase\CustomAssert;
use App\Controller\Component\PerformanceMemberServiceComponent;
use Cake\Controller\ComponentRegistry;

class MemberSummaryControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->PerformanceMemberService = new PerformanceMemberServiceComponent($registry);
    }

    public function testAllMembers() {
        $members = $this->getMemberSummary();
        $this->assertCount(3, $members);

        foreach ($members as $member) {
            CustomAssert::memberHasStandardSummary($member);
        }

        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    public function testAllMembersPerformanceCountIsInt() {
        $members = $this->getMemberSummary();

        foreach ($members as $member) {
            $this->assertTrue(is_int($member->performanceCount));
        }
    }

    public function testCurrentMembers() {
        $members = $this->getCurrentMemberSummary();
        $this->assertCount(2, $members);

        foreach ($members as $member) {
            CustomAssert::memberHasStandardSummary($member);
        }

        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    public function testCurrentMembersPerformanceCountIsInt() {
        $members = $this->getCurrentMemberSummary();

        foreach ($members as $member) {
            $this->assertTrue(is_int($member->performanceCount));
        }
    }

    public function testSeperatePerformanceActuallyMakesADifference() {
        $this->PerformanceMemberService->addPerformanceMembership(2, 1);

        $members = $this->getMemberSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 2);
    }

    public function testPerformanceInSameConcertDoesntMakeADifference() {
        $this->PerformanceMemberService->addPerformanceMembership(4, 1);

        $members = $this->getMemberSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    private function getCurrentMemberSummary() {
        $responseData = $this->get('/member/current/summary');
        $this->assertResponseOk();

        return $responseData->member;
    }

    private function getMemberSummary() {
        $responseData = $this->get('/member/summary');
        $this->assertResponseOk();

        return $responseData->member;
    }
}
