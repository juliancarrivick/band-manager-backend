<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\MemberServiceComponent;
use App\Controller\Component\EnsembleMembershipServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Cake\I18n\Time;
use App\Model\Table\MemberTable;
use Cake\ORM\TableRegistry;

class EnsembleMembershipServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.MemberInstrument',
        'app.Ensemble',
        'app.EnsembleMembership'
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->EnsembleMembershipService = new EnsembleMembershipServiceComponent($registry);
        $this->MemberService = new MemberServiceComponent($registry);

        $config = TableRegistry::exists('Member') ? [] : ['className' => 'App\Model\Table\MemberTable'];
        $this->Member = TableRegistry::get('Member', $config);
    }

    public function tearDown()
    {
        unset($this->EnsembleMembershipService);

        parent::tearDown();
    }

    public function testFindById()
    {
        $membership = $this->EnsembleMembershipService->findById(1);

        $this->assertNotNull($membership);
    }

    public function testFindNonExistantId()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->EnsembleMembershipService->findById(0);
    }

    public function testGetMembershipHistoryByMemberId()
    {
        $history = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(1);
        $this->assertCount(2, $history);
    }

    public function testGetMembershipHistoryByMemberIdNotExists()
    {
        $history = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(0);
        $this->assertCount(0, $history);
    }

    public function testGetCurrentMembersByEnsembleId()
    {
        $members = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId(1);
        $this->assertCount(1, $members);
    }

    public function testGetPastMembersByEnsembleId()
    {
        $members = $this->EnsembleMembershipService->getPastMembersByEnsembleId(1);
        $this->assertCount(1, $members);
    }

    public function testGetMembersByEnsemble()
    {
        $members = $this->EnsembleMembershipService->getMembersByEnsembleId(1);
        $this->assertCount(2, $members);
    }

    public function testAddMemberToEnsemble()
    {
        $beforeCount = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();
        $newMembership = $this->EnsembleMembershipService->addMembership([
            'memberId' => 3,
            'ensembleId' => 1
        ]);
        $afterCount = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();

        $this->assertNotNull($newMembership);
        $this->assertGreaterThan(0, $newMembership->id);
        $this->assertEquals($beforeCount + 1, $afterCount);
    }

    public function testAddAlreadyExistsToEnsemble()
    {
        $before = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();
        $existingMembership = $this->EnsembleMembershipService->addMembership([
            'memberId' => 1,
            'ensembleId' => 1
        ]);
        $after = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();

        $this->assertNotNull($existingMembership);
        $this->assertGreaterThan(0, $existingMembership->id);
        $this->assertEquals($before, $after);
    }

    public function testAddMemberToEnsembleDateJoinedGetsAutomaticallySet()
    {
        $this->EnsembleMembershipService->addMembership([
            'memberId' => 3,
            'ensembleId' => 1
        ]);
        $membership = $this->EnsembleMembershipService->findMembership(3, 1);

        $this->assertNotNull($membership->dateJoined);
    }

    public function testEndMembership()
    {
        $before = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId(1)->count();
        $this->EnsembleMembershipService->endMembership(1, 1);
        $after = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId(1)->count();

        $this->assertEquals($before - 1, $after);
    }

    public function testEndMembershipSetsDateLeft()
    {
        $membership = $this->EnsembleMembershipService->findMembership(1, 1);
        $this->EnsembleMembershipService->endMembership(1, 1);

        $membership = $this->EnsembleMembershipService->findById($membership->id);

        $this->assertNotNull($membership->dateLeft);
    }

    public function testEndMembershipNotExists()
    {
        $before = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId(1)->count();
        $this->EnsembleMembershipService->endMembership(3, 1);
        $after = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId(1)->count();

        $this->assertEquals($before, $after);
    }

    public function testEndLastMembershipAlsoSetsMemberDateLeft()
    {
        $member = $this->MemberService->findById(2);
        $this->assertNull($member->dateLeft);

        $this->EnsembleMembershipService->endMembership(2, 3);

        $member = $this->MemberService->findById(2);
        $this->assertNotNull($member->dateLeft);
    }

    public function testUpdateDateJoined()
    {
        $updateMap = ['dateJoined' => new Time('+1 day')];

        $membership = $this->EnsembleMembershipService->findById(1);
        $this->assertTrue($membership->dateJoined < new Time());

        $membership = $this->EnsembleMembershipService->update(1, $updateMap);
        $this->assertTrue($membership->dateJoined > new Time());
    }

    public function testUpdateDateJoinedNullIgnored()
    {
        $updateMap = ['dateJoined' => null];

        $membership = $this->EnsembleMembershipService->findById(1);
        $this->assertTrue($membership->dateJoined < new Time());

        $membership = $this->EnsembleMembershipService->update(1, $updateMap);
        $this->assertNotNull($membership->dateJoined);
    }

    public function testUpdateDateLeft()
    {
        $updateMap = ['dateLeft' => new Time('+1 day')];

        $membership = $this->EnsembleMembershipService->findById(1);
        $this->assertTrue($membership->dateLeft < new Time());

        $membership = $this->EnsembleMembershipService->update(1, $updateMap);
        $this->assertTrue($membership->dateLeft > new Time());
    }

    public function testUpdateDateLeftNullIgnored()
    {
        $updateMap = ['dateLeft' => null];

        $membership = $this->EnsembleMembershipService->findById(3);
        $this->assertTrue($membership->dateLeft < new Time());

        $membership = $this->EnsembleMembershipService->update(3, $updateMap);
        $this->assertNotNull($membership->dateLeft);
    }

    public function testUpdateCantUpdateMemberOdEnsemble()
    {
        $updateMap = [
            'memberId' => 3,
            'ensembleId' => 3
        ];

        $membership = $this->EnsembleMembershipService->update(1, $updateMap);
        $membership = $this->EnsembleMembershipService->findById(1);
        $this->assertNotEquals(3, $membership->assetId);
        $this->assertNotEquals(3, $membership->memberId);
    }

    public function testDeleteMembership()
    {
        $before = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();
        $this->EnsembleMembershipService->deleteMembershipById(1);
        $after = $this->EnsembleMembershipService->getMembersByEnsembleId(1)->count();

        $this->assertEquals($before - 1, $after);
    }

    public function testDeleteMembershipNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->EnsembleMembershipService->deleteMembershipById(0);
    }

    public function testSetMembersIdsMembershipToJoinDate()
    {
        $newDateJoined = new Time('2017-06-04');

        $this->EnsembleMembershipService->setMemberIdsMembershipToJoinDate(1, $newDateJoined);

        $membershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(1);
        foreach ($membershipHistory as $membership) {
            $this->assertEquals($newDateJoined, $membership->dateJoined);
        }
    }

    public function testInvalidJoinDate()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $this->EnsembleMembershipService->setMemberIdsMembershipToJoinDate(1, null);
    }

    public function testSetMembersIdsMembershipToLeftDateOnlyNullsGetSet()
    {
        $originalMembershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(2)->toArray();
        $newDateLeft = new Time('2017-06-04');

        $this->EnsembleMembershipService->setMemberIdsMembershipToLeftDate(2, $newDateLeft);

        $newMembershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(2)->toArray();
        for ($i = 0; $i < count($newMembershipHistory); $i++) {
            $originalDateLeft = $originalMembershipHistory[$i]->dateLeft;
            $dateLeftNow = $newMembershipHistory[$i]->dateLeft;

            if ($originalDateLeft) {
                $this->assertEquals($originalDateLeft, $dateLeftNow);
            } else {
                $this->assertEquals($newDateLeft, $dateLeftNow);
            }
        }
    }

    public function testSetMembersIdsMembershipToLeftDateAlsoUpdatesSameDateLeftAsMember()
    {
        $member = $this->MemberService->findById(3);
        $newMembership = $this->EnsembleMembershipService->addMembership([
            'memberId' => 3,
            'ensembleId' => 1
        ]);
        $membershipUpdate = [
            'dateLeft' => $member->dateLeft
        ];

        $this->EnsembleMembershipService->update($newMembership->id, $membershipUpdate);
        // Set this back too (it gets removed when the new membership is added)
        $member = $this->MemberService->findById(3);
        $this->Member->patchEntity($member, $membershipUpdate);
        $this->Member->save($member);

        $newDateLeft = new Time('2017-06-04');
        $this->EnsembleMembershipService->setMemberIdsMembershipToLeftDate(3, $newDateLeft);

        $updatedMembership = $this->EnsembleMembershipService->findById($newMembership->id);
        $this->assertEquals($newDateLeft, $updatedMembership->dateLeft);
    }

    public function testEmptyLeftDate()
    {
        $originalMembershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(1)->toArray();

        $this->EnsembleMembershipService->setMemberIdsMembershipToLeftDate(1, null);

        $membershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(1)->toArray();
        for ($i = 0; $i < count($membershipHistory); $i++) {
            $originalDateLeft = $originalMembershipHistory[$i]->dateLeft;
            $dateLeftNow = $membershipHistory[$i]->dateLeft;

            $this->assertEquals($originalDateLeft, $dateLeftNow);
        }
    }

    public function testRemoveLeftDateIfAddedToEnsemble()
    {
        $member = $this->MemberService->findById(3);
        $this->assertNotNull($member->dateLeft);

        $newMembership = $this->EnsembleMembershipService->addMembership([
            'memberId' => 3,
            'ensembleId' => 1
        ]);

        $member = $this->MemberService->findById(3);
        $this->assertNull($member->dateLeft);
    }
}
