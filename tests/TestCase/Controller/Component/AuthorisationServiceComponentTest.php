<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AuthorisationServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Http\ServerRequest;

class AuthorisationServiceComponentTest extends TestCase
{
    public $AuthorisationService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AuthorisationService = new AuthorisationServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->AuthorisationService);
        parent::tearDown();
    }

    public function testHasRoleExists()
    {
        $user = DomainObjectFactory::createUserMap();
        $user['roles'] = [
            ['id' => 1, 'role' => 'admin']
        ];

        $this->assertTrue($this->AuthorisationService->userHasRole($user, 'admin'));
    }

    public function testHasRoleViaEnsemble()
    {
        $user = DomainObjectFactory::createUserMap();
        $user['roles'] = [];
        $user['member']['ensemble'] = [
            [
                'id' => 1,
                'roles' => [
                    ['id' => 1, 'role' => 'admin'],
                ],
            ],
        ];

        $this->assertTrue($this->AuthorisationService->userHasRole($user, 'admin'));
    }

    public function testHasRoleDoesntExist()
    {
        $user = DomainObjectFactory::createUserMap();
        $user['roles'] = [
            ['id' => 1, 'role' => 'admin']
        ];

        $this->assertFalse($this->AuthorisationService->userHasRole($user, 'user'));
    }

    public function testViewerOnlyCanAccessGet()
    {
        $request = (new ServerRequest())
            ->withEnv('REQUEST_METHOD', 'GET');
        $user = DomainObjectFactory::createUserMap();
        $user['roles'] = [
            ['id' => 1, 'role' => 'viewer']
        ];

        $this->assertTrue($this->AuthorisationService->requestIsViewerAttemptingToRead($request, $user));
    }
}
