<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\PerformanceScoreServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

class PerformanceScoreServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.Ensemble',
        'app.Concert',
        'app.Score',
        'app.Performance',
        'app.PerformanceMember',
        'app.PerformanceScore',
    ];

    public $PerformanceScoreService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->PerformanceScoreService = new PerformanceScoreServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->PerformanceScoreService);

        parent::tearDown();
    }

    public function testFindByPerformanceId()
    {
        $scores = $this->PerformanceScoreService->findByPerformanceId(1);
        $this->assertCount(2, $scores);
    }

    public function testFindByPerformanceIdUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceScoreService->findByPerformanceId(0);
    }

    public function testFindByPerformanceIdAreScoreObjects()
    {
        $scores = $this->PerformanceScoreService->findByPerformanceId(1);

        foreach ($scores as $score) {
            $this->assertNotNull($score->title);
        }
    }

    public function testAddPerformanceScore()
    {
        $countBefore = $this->countScoresInPerformance(1);
        $this->PerformanceScoreService->addPerformanceScore(1, 3);
        $countAfter = $this->countScoresInPerformance(1);

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    private function countScoresInPerformance($performanceId)
    {
        $scores = $this->PerformanceScoreService->findByPerformanceId($performanceId);
        return $scores->count();
    }

    public function testAddPerformanceScoreNonExistantPerformance()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceScoreService->addPerformanceScore(0, 3);
    }

    public function testAddPerformanceScoreNonExistantScore()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceScoreService->addPerformanceScore(1, 0);
    }

    public function testAddPerformanceScoreAlreadyExists()
    {
        $countBefore = $this->countScoresInPerformance(1);
        $this->PerformanceScoreService->addPerformanceScore(1, 1);
        $countAfter = $this->countScoresInPerformance(1);

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testDeletePerformanceScore()
    {
        $countBefore = $this->countScoresInPerformance(1);
        $this->PerformanceScoreService->deletePerformanceScore(1, 1);
        $countAfter = $this->countScoresInPerformance(1);

        $this->assertEquals($countBefore - 1, $countAfter);
    }

    public function testDeletePerformanceScoreAlreadyDeleted()
    {
        $countBefore = $this->countScoresInPerformance(1);
        $this->PerformanceScoreService->deletePerformanceScore(1, 3);
        $countAfter = $this->countScoresInPerformance(1);

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testDeletePerformanceScoreNonExistantPerformance()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceScoreService->deletePerformanceScore(0, 3);
    }

    public function testDeletePerformanceScoreNonExistantScore()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceScoreService->deletePerformanceScore(1, 0);
    }
}
