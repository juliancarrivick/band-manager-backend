<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AssetServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\i18n\Time;

class AssetServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.Asset',
        'app.AssetLoan',
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AssetService = new AssetServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->AssetService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $assets = $this->AssetService->findAll();
        $this->assertCount(3, $assets);
    }

    public function testFindByIdExists()
    {
        $asset = $this->AssetService->findById(1);
        $this->assertEquals(1, $asset->id);
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $asset = $this->AssetService->findById(0);
    }

    public function testCreateValidAsset()
    {
        $asset = DomainObjectFactory::createAssetMap();

        $countBefore = $this->AssetService->count();
        $this->AssetService->create($asset);
        $countAfter = $this->AssetService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testCreateInvalidAsset()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $asset = DomainObjectFactory::createAssetMap();
        $asset['description'] = ''; // Empty is invalid

        $countBefore = $this->AssetService->count();
        $this->AssetService->create($asset);
        $countAfter = $this->AssetService->count();

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testUpdateValidAsset()
    {
        $asset = $this->AssetService->findById(1);
        $asset->description = 'Drum Set';

        $this->AssetService->update(1, $asset->toArray());

        $this->assertEquals('Drum Set', $this->AssetService->findById(1)->description);
    }

    public function testUpdateInvalidAsset()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $asset = $this->AssetService->findById(1);
        $asset->description = '';

        $this->AssetService->update(1, $asset->toArray());
    }

    public function testDeleteAsset()
    {
        $this->AssetService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetService->delete(0);
    }

    public function testGetAllHasCurrentLoan()
    {
        $assets = $this->AssetService->findAll();

        // One of the assets will have a current loan, but we don't know which one
        $found = false;
        foreach ($assets as $asset) {
            if ($asset->loanedTo && $asset->loanedTo->member) {
                $found = true;
                break;
            }
        }

        $this->assertTrue($found);
    }

    public function testGetHasCurrentLoan()
    {
        $assetWithCurrentLoan = $this->AssetService->findById(2);

        $this->assertNotNull($assetWithCurrentLoan->loanedTo);
        $this->assertNotNull($assetWithCurrentLoan->loanedTo->member);
    }

    public function testGetWithoutCurrentLoan()
    {
        $assetWithoutCurrentLoan = $this->AssetService->findById(1);
        $this->assertNull($assetWithoutCurrentLoan->loanedTo);
    }
}
