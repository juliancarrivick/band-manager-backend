<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ConcertController;
use App\Controller\Component\ConcertServiceComponent;
use Cake\Controller\ComponentRegistry;
use App\Test\TestCase\DomainObjectFactory;

class ConcertControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->ConcertService = new ConcertServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->ConcertService);

        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/concert');

        $this->assertResponseOk();
        $this->assertResponseContains('concert');
        $this->assertCount(3, $responseData->concert);
    }

    public function testViewExists() {
        $responseData = $this->get('/concert/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->concert->id);
    }

    public function testViewNotExists() {
        $this->get('/concert/0');

        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newConcert = DomainObjectFactory::createConcertMap();
        unset($newConcert['id']);
        $newConcert['occasion'] = 'Christmas!';

        $responseData = $this->post('/concert', $newConcert);

        $this->assertResponseOk();
        $this->assertEquals('Christmas!', $responseData->concert->occasion);
        $this->assertGreaterThan(0, $responseData->concert->id);

        // Make sure it was actually created.
        $this->ConcertService->findById($responseData->concert->id);
    }

    public function testAddInvalid() {
        $newConcert = DomainObjectFactory::createConcertMap();
        $newConcert['date'] = 'INVALID DATE';

        $countBefore = $this->ConcertService->count();
        $this->post('/concert', $newConcert);
        $countAfter = $this->ConcertService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testEdit() {
        $modifiedConcert = $this->ConcertService->findById(1);
        $modifiedConcert->occasion = 'Christmas!';

        $responseData = $this->put('/concert/1', $modifiedConcert->toArray());

        $this->assertResponseOk();
        $this->assertEquals('Christmas!', $responseData->concert->occasion);
        $this->assertEquals(1, $responseData->concert->id);

        // Make sure it was actually updated.
        $dbConcert = $this->ConcertService->findById($responseData->concert->id);
        $this->assertEquals('Christmas!', $dbConcert->occasion);
    }

    public function testEditInvalid() {
        $modifiedConcert = $this->ConcertService->findById(1);
        $modifiedConcert->date = 'INVALID DATE';

        $this->put('/concert/1', $modifiedConcert->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbConcert = $this->ConcertService->findById(1);
        $this->assertNotEquals('Bass Clarinet', $dbConcert->occasion);
    }

    public function testEditNotExists() {
        $modifiedConcert = $this->ConcertService->findById(1);
        $modifiedConcert->occasion = 'Christmas!';

        $this->put('/concert/0', $modifiedConcert->toArray());

        $this->assertResponseCode(404);
    }

    public function testDeleteExists() {
        $this->delete('/concert/1');

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->ConcertService->findById(1);
    }

    public function testDeleteNotExist() {
        $this->delete('/concert/0');
        $this->assertResponseCode(404);
    }

    public function testGetByMemberId() {
        $concerts = $this->get('/member/1/concert')->concert;
        $this->assertResponseOk();

        $this->assertCount(1, $concerts);
        $this->assertEquals(1, $concerts[0]->id);
        $this->assertCount(1, $concerts[0]->performance);
        $this->assertEquals(1, $concerts[0]->performance[0]->id);
        $this->assertEquals(1, $concerts[0]->performance[0]->ensemble->id);
    }
}
