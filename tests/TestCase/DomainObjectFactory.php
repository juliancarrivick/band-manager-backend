<?php
namespace App\Test\TestCase;

class DomainObjectFactory
{
    public static function createAssetMap()
    {
        return [
            'id' => 1,
            'description' => 'Bb Clarinet',
            'serialNo' => 'XYZ',
            'dateAcquired' => '2015-11-17T01:16:48Z',
            'valuePaid' => '1000',
            'dateDiscarded' => '2015-11-17T01:16:48Z',
            'valueDiscarded' => '',
            'insuranceValue' => '1000',
            'currentLocation' => 'Scout Hall',
            'notes' => ''
        ];
    }

    public static function createUserMap()
    {
        return [
            'id' => 1,
            'username' => 'test1',
            'password' => 'test1',
            'created' => '2015-12-14T08:56:23Z',
            'modified' => '2015-12-14T08:56:23Z',
            'roles' => [
                ['id' => 1, 'role' => 'admin'],
                ['id' => 2, 'role' => 'editor']
            ]
        ];
    }

    public static function createMemberMap()
    {
        return [
            'id' => 1,
            'firstName' => 'Bob',
            'lastName' => 'Marley',
            'dateJoined' => '2016-11-17T01:16:48Z',
            'phoneNo' => '+61400000000',
            'email' => 'bob@marley.com',
            'address' => '1 Home Street',
            'feeClass' => 'Concession',
            'membershipClass' => 'Ordinary',
            'isCurrent' => 1,
            'notes' => 'Great Reggae'
        ];
    }

    public static function createEnsembleMap()
    {
        return [
            'id' => 1,
            'name' => 'Clarinet Ensemble',
            'isHidden' => false
        ];
    }

    public static function createScoreMap()
    {
        return [
            'id' => 1,
            'title' => 'Bohemian Rhapsody',
            'composer' => 'Queen',
            'arranger' => '',
            'genre' => 'Rock',
            'grade' => 3,
            'duration' => 582,
            'datePurchased' => '2016-09-19T07:04:52Z',
            'valuePaid' => 100.90,
            'location' => 'Scout Hall',
            'notes' => 'Great Song'
        ];
    }

    public static function createConcertMap()
    {
        return [
            'id' => 1,
            'occasion' => 'State Championships',
            'location' => 'Christchurch Grammer School',
            'date' => '2016-10-08T05:27:29Z',
            'notes' => 'We won!'
        ];
    }

    public static function createAssetLoanMap()
    {
        return [
            'id' => 1,
            'memberId' => 1,
            'assetId' => 1,
            'dateBorrowed' => '2016-09-19T07:04:52Z'
        ];
    }

    public static function createEnsembleMembershipMap()
    {
        return [
            'id' => 1,
            'memberId' => 1,
            'ensembleId' => 1
        ];
    }

    public static function createMemberInstrumentMap()
    {
        return [
            'id' => 1,
            'memberId' => 1,
            'instrumentName' => 'Clarinet'
        ];
    }

    public static function createPerformanceMap()
    {
        return [
            'id' => 1,
            'concertId' => 1,
            'ensembleId' => 1,
            'countsAsSeperate' => false
        ];
    }

    public static function createPerformanceMember()
    {
        return [
            'id' => 1,
            'performanceId' => 1,
            'memberId' => 1
        ];
    }
}
