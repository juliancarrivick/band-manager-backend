<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ScoreFixture
 *
 */
class ScoreFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'score';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 90, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'composer' => ['type' => 'string', 'length' => 90, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'arranger' => ['type' => 'string', 'length' => 90, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'genre' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'grade' => ['type' => 'decimal', 'length' => 2, 'precision' => 1, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'duration' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'datePurchased' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'valuePaid' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'location' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'notes' => ['type' => 'string', 'length' => 2000, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => 'Bohemian Rhapsody',
            'composer' => 'Queen',
            'arranger' => '',
            'genre' => 'Rock',
            'grade' => 3,
            'duration' => 582,
            'datePurchased' => '2016-09-19 07:04:52',
            'valuePaid' => 100.90,
            'location' => 'Scout Hall',
            'notes' => 'Great Song'
        ],
        [
            'id' => 2,
            'title' => 'The Power of Love',
            'composer' => 'Huey Lewis and the News',
            'arranger' => '',
            'genre' => 'Rock',
            'grade' => 4,
            'duration' => 256,
            'datePurchased' => '2016-09-19 07:04:52',
            'valuePaid' => 200.90,
            'location' => 'Leeming High School',
            'notes' => 'From a great movie'
        ],
        [
            'id' => 3,
            'title' => 'Nothing Else Matters',
            'composer' => 'Metallica',
            'arranger' => '',
            'genre' => 'Rock',
            'grade' => 2.5,
            'duration' => 212,
            'datePurchased' => '2016-09-19 07:04:52',
            'valuePaid' => 50.90,
            'location' => 'Scout Hall',
            'notes' => 'Also Great Song'
        ],
    ];
}
