<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EnsembleRoleFixture
 */
class EnsembleRoleFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ensemble_role';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ensembleId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'roleId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'ensembleId' => ['type' => 'index', 'columns' => ['ensembleId'], 'length' => []],
            'roleId' => ['type' => 'index', 'columns' => ['roleId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'ensemble_role_ibfk_1' => ['type' => 'foreign', 'columns' => ['ensembleId'], 'references' => ['ensemble', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'ensemble_role_ibfk_2' => ['type' => 'foreign', 'columns' => ['roleId'], 'references' => ['role', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'ensembleId' => 1,
                'roleId' => 1
            ],
            [
                'id' => 2,
                'ensembleId' => 3,
                'roleId' => 1
            ],
        ];
        parent::init();
    }
}
