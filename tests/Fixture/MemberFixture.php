<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MemberFixture
 *
 */
class MemberFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'member';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'firstName' => ['type' => 'string', 'length' => 35, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'lastName' => ['type' => 'string', 'length' => 35, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'phoneNo' => ['type' => 'string', 'fixed' => true, 'length' => 12, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'email' => ['type' => 'string', 'fixed' => true, 'length' => 90, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'address' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'feeClass' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => 'Standard', 'comment' => '', 'precision' => null, 'fixed' => null],
        'membershipClass' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => 'Ordinary', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dateJoined' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dateLeft' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'notes' => ['type' => 'string', 'length' => 1000, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'userId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'userId' => ['type' => 'index', 'columns' => ['userId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'member_ibfk_1' => ['type' => 'foreign', 'columns' => ['userId'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'email_UNIQUE' => ['type' => 'unique', 'columns' => ['email'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'firstName' => 'Bob',
            'lastName' => 'Marley',
            'phoneNo' => '+61400000000',
            'email' => 'bmarley@domain.com',
            'address' => '1 Home Street',
            'feeClass' => 'Concession',
            'membershipClass' => 'Ordinary',
            'dateJoined' => '2015-11-17 01:16:48',
            'dateLeft' => null,
            'notes' => 'Great Reggae',
            'userId' => 1,
        ],
        [
            'id' => 2,
            'firstName' => 'Glenn',
            'lastName' => 'Stevens',
            'phoneNo' => '+61411111111',
            'email' => 'gstevens@domain.com',
            'address' => '5 Iggy Street',
            'feeClass' => 'Full',
            'membershipClass' => 'Ordinary',
            'dateJoined' => '2015-11-17 01:16:48',
            'dateLeft' => null,
            'notes' => 'Don\'t know who he is',
            'userId' => 2,
        ],
        [
            'id' => 3,
            'firstName' => 'Iggy',
            'lastName' => 'Pop',
            'phoneNo' => '+61433333333',
            'email' => 'iggy@pop.com',
            'address' => '2 Infinity Way',
            'feeClass' => 'Special',
            'membershipClass' => 'Ordinary',
            'dateJoined' => '2015-11-17 01:16:48',
            'dateLeft' => '2015-11-19 01:16:48',
            'notes' => 'Somehow still alive'
        ],
    ];
}
