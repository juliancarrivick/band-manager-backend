<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AssetFixture
 *
 */
class AssetFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'asset';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'description' => ['type' => 'string', 'length' => 1000, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => 1, 'comment' => '', 'precision' => null],
        'serialNo' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'dateAcquired' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'valuePaid' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'dateDiscarded' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'valueDiscarded' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'insuranceValue' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'currentLocation' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'notes' => ['type' => 'string', 'length' => 2000, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'description' => 'Bb Clarinet',
            'quantity' => 1,
            'serialNo' => 'XYZ',
            'dateAcquired' => '2015-11-17 01:16:48',
            'valuePaid' => '1000',
            'dateDiscarded' => '2015-11-17 01:16:48',
            'valueDiscarded' => '',
            'insuranceValue' => '1000',
            'currentLocation' => 'Scout Hall',
            'notes' => ''
        ],
        [
            'description' => 'Bb Trumpet',
            'quantity' => 1,
            'serialNo' => '123ABC',
            'dateAcquired' => '2015-11-17 01:16:48',
            'valuePaid' => '2000',
            'dateDiscarded' => '2015-11-17 01:16:48',
            'valueDiscarded' => '',
            'insuranceValue' => '1500',
            'currentLocation' => 'On Loan',
            'notes' => ''
        ],
        [
            'description' => 'Bass Drum',
            'quantity' => 1,
            'serialNo' => 'HIJK',
            'dateAcquired' => '2015-11-17 01:16:48',
            'valuePaid' => '1500',
            'dateDiscarded' => '2015-11-17 01:16:48',
            'valueDiscarded' => '',
            'insuranceValue' => '1000',
            'currentLocation' => 'School',
            'notes' => 'Broken Wheel'
        ]
    ];
}
