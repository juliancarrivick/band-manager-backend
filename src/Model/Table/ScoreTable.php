<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use \ArrayObject;
use Cake\Event\Event;

/**
 * Score Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Performance
 *
 * @method \App\Model\Entity\Score get($primaryKey, $options = [])
 * @method \App\Model\Entity\Score newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Score[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Score|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Score patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Score[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Score findOrCreate($search, callable $callback = null)
 */
class ScoreTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('AutoDateConvert', [
            'dateTimeFields' => ['datePurchased']
        ]);

        $this->setTable('score');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Performance', [
            'foreignKey' => 'scoreId',
            'targetForeignKey' => 'performanceId',
            'joinTable' => 'performance_score'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('composer');

        $validator
            ->allowEmpty('arranger');

        $validator
            ->allowEmpty('genre');

        $validator
            ->decimal('valuePaid')
            ->allowEmpty('grade');

        $validator
            ->integer('duration')
            ->allowEmpty('duration');

        $validator
            ->dateTime('datePurchased')
            ->allowEmpty('datePurchased');

        $validator
            ->decimal('valuePaid')
            ->allowEmpty('valuePaid');

        $validator
            ->allowEmpty('location');

        $validator
            ->allowEmpty('notes');

        return $validator;
    }
}
