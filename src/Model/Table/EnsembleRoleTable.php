<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EnsembleRole Model
 *
 * @method \App\Model\Entity\EnsembleRole get($primaryKey, $options = [])
 * @method \App\Model\Entity\EnsembleRole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EnsembleRole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EnsembleRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnsembleRole saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnsembleRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EnsembleRole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EnsembleRole findOrCreate($search, callable $callback = null, $options = [])
 */
class EnsembleRoleTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ensemble_role');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('ensembleId')
            ->requirePresence('ensembleId', 'create')
            ->allowEmptyString('ensembleId', false);

        $validator
            ->integer('roleId')
            ->requirePresence('roleId', 'create')
            ->allowEmptyString('roleId', false);

        return $validator;
    }
}
