<?php
namespace App\Model\Table;

use App\Model\Entity\Member;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use \ArrayObject;
use Cake\Event\Event;

/**
 * Member Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Ensemble
 * @property \Cake\ORM\Association\BelongsToMany $Performance
 */
class MemberTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('AutoDateConvert', [
            'dateTimeFields' => ['dateJoined', 'dateLeft']
        ]);

        $this->setTable('member');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('MemberInstrument', [
            'foreignKey' => 'memberId',
            'dependent' => true,
            'propertyName' => 'instruments',
        ]);

        $this->belongsToMany('Ensemble', [
            'foreignKey' => 'memberId',
            'targetForeignKey' => 'ensembleId',
            'through' => 'EnsembleMembership'
        ]);
        $this->hasMany('EnsembleMembership', [
            'foreignKey' => 'memberId',
            'propertyName' => 'membership',
        ]);

        $this->belongsToMany('Performance', [
            'foreignKey' => 'memberId',
            'targetForeignKey' => 'performanceId',
            'joinTable' => 'performance_member'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('firstName', 'create')
            ->notEmpty('firstName');

        $validator
            ->requirePresence('lastName', 'create')
            ->notEmpty('lastName');

        $validator
            ->add('phoneNo', 'valid', ['rule' => ['lengthBetween', 12, 12]])
            ->allowEmpty('phoneNo');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->allowEmpty('feeClass');

        $validator
            ->requirePresence('membershipClass', 'create')
            ->notEmpty('membershipClass');

        $validator
            ->add('dateJoined', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('dateJoined');

        $validator
            ->add('dateLeft', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('dateLeft');

        $validator
            ->allowEmpty('notes');

        return $validator;
    }
}
