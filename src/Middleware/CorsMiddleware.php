<?php

namespace App\Middleware;

use Cake\Core\Configure;

class CorsMiddleware
{
    public function __invoke($request, $response, $next)
    {
        // Access-Control headers are received during OPTIONS requests
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            return CorsMiddleware::createOptionsResponse($response);
        }

        $response = $next($request, $response);
        $response = CorsMiddleware::setCorsHeaders($response);
        return $response;
    }

    private static function createOptionsResponse($response)
    {
        $response = CorsMiddleware::setCorsHeaders($response);

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
            $response = $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        }


        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            $response = $response->withHeader('Access-Control-Allow-Headers', $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
        }

        $response = $response->withoutHeader('Location');
        $response = $response->withStatus(200);
        return $response;
    }

    private static function setCorsHeaders($response)
    {
        if (!isset($_SERVER['HTTP_ORIGIN'])) {
            return $response;
        }

        $origin = Configure::read('App.frontEndUri');
        return $response->withHeader('Access-Control-Allow-Origin', $origin)
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Max-Age', '86400');    // cache for 1 day
    }
}
