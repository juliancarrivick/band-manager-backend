<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController {
    public $components = [
        'UsersService'
    ];

    public function isAuthorized($user) {
        return $this->AuthorisationService->userHasRole($user, 'admin');
    }

    public function index() {
        $users = $this->UsersService->findAll();

        $this->set('user', $users);
        $this->set('_serialize', ['user']);
    }

    public function view($id = null) {
        $user = $this->UsersService->findById($id);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function add() {
        $this->request->allowMethod(['post']);

        $user = $this->UsersService->create($this->request->getData());

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function edit($id = null) {
        $this->request->allowMethod(['put']);

        $user = $this->UsersService->update($id, $this->request->getData());

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['delete']);

        $this->UsersService->delete($id);
    }
}
