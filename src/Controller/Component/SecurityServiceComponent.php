<?php

namespace App\Controller\Component;

use Cake\Cache\Cache;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\Time;
use Cake\Utility\Security;

class SecurityServiceComponent extends Component
{
    public $components = [
        'MemberService',
        'EmailService',
        'UsersService',
        'RoleService',
    ];

    public function resetPassword(string $encodedEmail, string $token, string $password)
    {
        $email = urldecode($encodedEmail);
        $member = $this->MemberService->findByEmail($email);
        if (!$member) {
            throw new BadRequestException('Invalid email or token2');
        }

        $cachedToken = Cache::read($email, 'app_password_token');
        if (!$cachedToken || $cachedToken !== $token) {
            throw new BadRequestException('Invalid email or token');
        }

        if (strlen($password) < 10) {
            throw new BadRequestException('Password must be at least 10 characters');
        }

        if ($member->userId) {
            $this->UsersService->update($member->userId, [
                'password' => $password,
            ]);
        } else {
            $newUser = $this->UsersService->create([
                'username' => $member->email,
                'password' => $password,
            ]);
            $this->MemberService->update($member->id, [
                'userId' => $newUser->id,
            ]);
        }

        Cache::delete($email, 'app_password_token');
    }

    public function requestPasswordReset(string $email)
    {
        $member = $this->MemberService->findByEmail($email);
        if (!$member) {
            return;
        }

        $this->EmailService->send([
            'toEmail' => $email,
            'toName' => "$member->firstName $member->lastName",
            'subject' => 'Band Manager Password Reset',
            'content' => $this->resetEmailContent($member),
        ]);
    }

    private function generateAndStoreToken(string $email)
    {
        if (!$email) {
            throw new BadRequestException('Member must have an email address');
        }

        $resetToken = Security::randomString();
        Cache::write($email, $resetToken, 'app_password_token');

        return $resetToken;
    }

    private function resetEmailContent($member)
    {
        $resetUrl = $this->generateResetLink($member->email);
        $formattedExpireTime = $this->getTokenExpiryEstimate();

        return <<<CONTENT
Hi $member->firstName,

We recently received a request to reset your password.

If this was not you, please disregard this email.

Otherwise, please click this link to reset your password: $resetUrl

This link will expire in $formattedExpireTime.
CONTENT;
    }

    private function generateResetLink(string $email)
    {
        $frontEndUri = Configure::read("App.frontEndUri");
        $encodedEmail = urlencode($email);

        $token = $this->generateAndStoreToken($email);
        $encodedToken = urlencode($token);

        $cacheExpiry = Configure::read('App.passwordTokenDuration');
        $expireTime = new Time($cacheExpiry);
        $expireTimestamp = $expireTime->format('c');
        $encodedTimestamp = urlencode($expireTimestamp);

        return "$frontEndUri/reset_password?email=$encodedEmail&token=$encodedToken&expiry=$encodedTimestamp";
    }

    public function inviteMemberById(int $memberId)
    {
        $member = $this->MemberService->findById($memberId);
        if (!$member) {
            throw new BadRequestException('Unknown member');
        }

        $orgName = Configure::read('App.organisationName');
        $this->EmailService->send([
            'toEmail' => $member->email,
            'toName' => "$member->firstName $member->lastName",
            'subject' => "Welcome to $orgName",
            'content' => $this->inviteEmailContent($member),
        ]);
    }

    private function inviteEmailContent($member)
    {
        $orgName = Configure::read('App.organisationName');
        $resetUrl = $this->generateResetLink($member->email);
        $formattedExpireTime = $this->getTokenExpiryEstimate();

        return <<<CONTENT
Hi $member->firstName,

You have been invited to join the band management system for the $orgName.

If you are not part of this organisation, please disregard this email.

Otherwise you may set a password and log in by clicking this link: $resetUrl

This link will expire in $formattedExpireTime.
CONTENT;
    }

    private function getTokenExpiryEstimate()
    {
        $cacheExpiry = Configure::read('App.passwordTokenDuration');
        $expireTime = new Time($cacheExpiry);
        $formattedExpireTime = $expireTime->timeAgoInWords([
            'accuracy' => 'hour'
        ]);

        return $formattedExpireTime;
    }
}
