<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\i18n\Time;

class MemberServiceComponent extends Component
{
    protected $_defaultConfig = [];

    public $components = [
        'MemberInstrumentService',
        'EnsembleMembershipService',
        'UsersService',
        'UtilityService',
    ];

    private $memberTable;

    public function initialize(array $config)
    {
        parent::initialize($config);

        /* Get the Member DAO/Table Object */
        $this->memberTable = TableRegistry::get('Member');
    }

    public function findAll()
    {
        return $this->memberTable->find()
            ->contain([
                'MemberInstrument',
                'Ensemble' => $this->containActiveEnsembleOnly()
            ]);
    }

    private function containActiveEnsembleOnly()
    {
        return function ($q) {
            return $q->where(['OR' => [
                'EnsembleMembership.dateLeft IS NULL',
                'EnsembleMembership.dateLeft >' => new Time()
            ]]);
        };
    }

    public function findById($id = null)
    {
        return $this->memberTable->get($id, [
            'contain' => [
                'MemberInstrument',
                'Ensemble' => $this->containActiveEnsembleOnly()
            ]
        ]);
    }

    public function findByEmail(string $email)
    {
        return $this->memberTable->find()
            ->where(['email' => $email])
            ->first();
    }

    public function currentMembers()
    {
        return $this->memberTable->find()
            ->where([
                'OR' => [
                    'dateLeft IS NULL',
                    'dateLeft >' => new Time(),
                ],
            ])
            ->contain([
                'MemberInstrument',
                'Ensemble' => $this->containActiveEnsembleOnly()
            ])
            ->distinct();
    }

    public function create($memberMap)
    {
        if (isset($memberMap['email'])) {
            $existingMember = $this->findByEmail($memberMap['email']);
            if ($existingMember) {
                throw new BadRequestException("A person with this email address already exists");
            }
        }

        // Attempt to map the map to an asset
        $memberMap = $this->prepareMemberMap($memberMap);
        $member = $this->memberTable->newEntity($memberMap);

        $this->saveMember($member);
        $this->MemberInstrumentService->setMembersInstrumentsFromMemberMap($member->id, $memberMap);
        $this->EnsembleMembershipService->setMemberIdsMembershipToJoinDate($member->id, $member->dateJoined);
        $this->EnsembleMembershipService->setMemberIdsMembershipToLeftDate($member->id, $member->dateLeft);

        // We have to do another call to populate all the associations
        return $this->findById($member->id);
    }

    private function prepareMemberMap($memberMap)
    {
        $toIdFields = [
            'ensembles' => 'ensemble'
        ];

        $memberMap = $this->UtilityService->convertMapToIdUpdateStructure($memberMap, $toIdFields);

        if (!isset($memberMap['dateJoined'])) {
            $memberMap['dateJoined'] = new Time();
        }

        return $memberMap;
    }

    private function saveMember($member)
    {
        if ($member->getErrors()) {
            throw new BadRequestException($member->getErrors());
        }

        if (!$this->memberTable->save($member)) {
            throw new InternalErrorException("Could not save Member.");
        }
    }

    public function update($id = null, $memberMap = null)
    {
        // Attempt to map the map to an asset
        $member = $this->findById($id);

        $member = $this->memberTable->patchEntity($member, $memberMap);

        // Only do this if the dateLeft is being updated
        if (isset($memberMap['dateLeft'])) {
            $this->EnsembleMembershipService->setMemberIdsMembershipToLeftDate($member->id, $member->dateLeft);
        }

        if (isset($memberMap['email']) && $member->userId) {
            $this->UsersService->update($member->userId, ['username' => $member->email]);
        }

        $this->saveMember($member);

        return $member;
    }

    public function delete($id = null)
    {
        $member = $this->findById($id);

        $this->MemberInstrumentService->removeMembersInstruments($id);

        if ($member->userId) {
            $this->UsersService->delete($member->userId);
        }

        if (!$this->memberTable->delete($member)) {
            throw new BadRequestException('Couldn\'t delete asset.');
        }
    }

    public function count()
    {
        return $this->memberTable->find()->count();
    }

    public function getAllMembersSummary()
    {
        $query = $this->findAll();
        $query = $this->embedPerformanceCount($query);
        $query = $this->stripPersonalInformationFromMemberQuery($query);

        return $query;
    }

    private function embedPerformanceCount($query)
    {
        $performanceCount = $this->getSeperatePerformanceCount($this->findAll());
        $concertCount = $this->getConcertCount($this->findAll());

        $subquery = $query
            ->select([
                'performanceCount' => 'Concert.concertPerformanceCount + Performance.seperatePerformanceCount'
            ])->innerJoin(
                ['Concert' => $concertCount],
                ['Concert.Member__id = Member.id']
            )->innerJoin(
                ['Performance' => $performanceCount],
                ['Performance.Member__id = Member.id']
            );

        $subquery->getSelectTypeMap()
            ->addDefaults([
                'performanceCount' => 'integer'
            ]);

        return $subquery;
    }

    private function getConcertCount($query)
    {
        return $query->select([
            'Member.id',
            'concertPerformanceCount' => $query->func()->count('DISTINCT Performance.concertId')
        ])
            ->leftJoinWith('Performance', function ($q) {
                return $q->where(['Performance.countsAsSeperate' => false]);
            })
            ->group('Member.id');
    }

    private function getSeperatePerformanceCount($query)
    {
        return $query->select([
            'Member.id',
            'seperatePerformanceCount' => $query->func()->count('Performance.id')
        ])
            ->leftJoinWith('Performance', function ($q) {
                return $q->where(['Performance.countsAsSeperate' => true]);
            })
            ->group('Member.id');
    }

    private function stripPersonalInformationFromMemberQuery($query)
    {
        return $query->select(['id', 'firstName', 'lastName']);
    }

    public function getCurrentMemberSummary()
    {
        $query = $this->currentMembers();
        $query = $this->embedPerformanceCount($query);
        $query = $this->stripPersonalInformationFromMemberQuery($query);

        return $query;
    }
}
