<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Datasource\Exception\RecordNotFoundException;

class ScoreServiceComponent extends Component {
    protected $_defaultConfig = [];

    private $scoreTable;

    public function initialize(array $config) {
        parent::initialize($config);
        $this->scoreTable = TableRegistry::get('Score');
    }

    public function findAll() {
        $query = $this->scoreTable->find();
        $query->select(['playCount' => $query->func()->count('Performance.id')])
            ->leftJoinWith('Performance')
            ->group('Score.id')
            ->enableAutoFields(true);

        return $query;
    }

    public function findById($id) {
        $score = $this->findAll()
            ->where(['Score.id' => $id])
            ->first();

        if (!$score) {
            throw new RecordNotFoundException('Score not found');
        }

        return $score;
    }

    public function create($scoreMap) {
        $score = $this->scoreTable->newEntity($scoreMap);

        if ($score->getErrors()) {
            throw new BadRequestException(print_r($score->getErrors(), true));
        }

        if (!$this->scoreTable->save($score)) {
            throw new BadRequestException("Invalid Score");
        }

        return $this->findById($score->id);
    }

    public function update($id, $scoreMap) {
        // Attempt to map the map to an asset
        $score = $this->findById($id);

        try {
            $score = $this->scoreTable->patchEntity($score, $scoreMap);
        } catch (\Exception $e) {
            throw new BadRequestException($e->getMessage());
        }

        if ($score->getErrors()) {
            throw new BadRequestException(print_r($score->getErrors(), true));
        }

        if (!$this->scoreTable->save($score)) {
            throw new InternalErrorException("Unable to save Score.");
        }

        return $score;
    }

    public function delete($id) {
        $score = $this->findById($id);

        if (!$this->scoreTable->delete($score)) {
            throw new InternalErrorException('Couldn\'t delete score.');
        }
    }

    public function count() {
        return $this->scoreTable->find()->count();
    }
}
