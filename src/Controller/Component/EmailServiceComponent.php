<?php

namespace App\Controller\Component;

use App\Controller\Component\Email\EmailProvider;
use App\Controller\Component\Email\MailjetEmailProvider;
use App\Controller\Component\Email\SendGridEmailProvider;
use Cake\Controller\Component;
use Cake\Core\Configure;

class EmailServiceComponent extends Component
{
    private $overrideProvider;

    protected $_defaultConfig = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function setOverrideProvider(EmailProvider $provider)
    {
        $this->overrideProvider = $provider;
    }

    public function send($emailData)
    {
        $toEmailOverride = Configure::read('App.toEmailOverride');

        // Don't just check for existance as empty string will not trigger override
        // Empty string will disable send
        if ($toEmailOverride !== null) {
            $emailData['toEmail'] = $toEmailOverride;
        }

        $provider = $this->getProvider($emailData);
        $provider->send($emailData);
    }

    private function getProvider($emailData)
    {
        if ($this->overrideProvider) {
            return $this->overrideProvider;
        }

        if (strpos($emailData['toEmail'], 'hotmail') !== false) {
            return new MailjetEmailProvider();
        }

        return new SendGridEmailProvider();
    }
}
