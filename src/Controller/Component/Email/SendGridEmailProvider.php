<?php
namespace App\Controller\Component\Email;

use App\Controller\Component\Email\EmailProvider;
use Cake\Core\Configure;

class SendgridEmailProvider implements EmailProvider
{
    private $sendGrid;

    public function __construct()
    {
        $apiKey = Configure::read('App.sendgridApiKey');
        $this->sendGrid = new \SendGrid($apiKey);
    }

    public function send($emailData)
    {
        $email = new \SendGrid\Mail\Mail();

        $fromName = Configure::read('App.organisationName');
        $email->setFrom(Configure::read('App.defaultFromEmail'), $fromName);

        $email->addTo($emailData['toEmail'], $emailData['toName']);
        $email->setSubject($emailData['subject']);
        $email->addContent("text/plain", $emailData['content']);

        $this->sendGrid->send($email);
    }
}
