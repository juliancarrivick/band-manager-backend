<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\InternalErrorException;
use Cake\ORM\TableRegistry;

class RoleServiceComponent extends Component
{
    public $components = [
        'UsersService',
        'EnsembleService',
    ];

    private $roleTable;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->roleTable = TableRegistry::get('Role');
        $this->userRoleTable = TableRegistry::get('UserRole');
        $this->ensembleRoleTable = TableRegistry::get('EnsembleRole');
    }

    public function findAll()
    {
        return $this->roleTable->find();
    }

    public function findById($roleId)
    {
        return $this->roleTable->get($roleId);
    }

    public function findByName(string $name)
    {
        return $this->roleTable->find()
            ->where(['role' => $name])
            ->first();
    }

    public function findByUserId($userId)
    {
        $this->UsersService->findById($userId);

        return $this->roleTable->find()
            ->innerJoinWith('Users', function ($q) use ($userId) {
                return $q->where(['Users.id' => $userId]);
            });
    }

    public function findByEnsembleId($ensembleId)
    {
        $this->EnsembleService->findById($ensembleId);

        return $this->roleTable->find()
            ->innerJoinWith('Ensemble', function ($q) use ($ensembleId) {
                return $q->where(['Ensemble.id' => $ensembleId]);
            });
    }

    public function addUserToRoleByName(int $userId, string $roleName)
    {
        $role = $this->findByName($roleName);
        if (!$role) {
            throw new RecordNotFoundException("Could not find role: $roleName");
        }

        $this->addUserToRole($userId, $role->id);
    }

    public function addUserToRole(int $userId, int $roleId)
    {
        if (!$this->userRoleExists($userId, $roleId)) {
            $userRole = $this->userRoleTable->newEntity();
            $userRole->userId = $userId;
            $userRole->roleId = $roleId;

            if (!$this->userRoleTable->save($userRole)) {
                throw new InternalErrorException('Unable to save user role');
            }
        }
    }

    private function userRoleExists($userId, $roleId)
    {
        return $this->getUserRole($userId, $roleId) != null;
    }

    private function getUserRole($userId, $roleId)
    {
        $this->findById($roleId);
        $this->UsersService->findById($userId);

        return $this->userRoleTable->find()
            ->where([
                'userId' => $userId,
                'roleId' => $roleId,
            ])
            ->first();
    }

    public function removeUserFromRole($userId, $roleId)
    {
        $userRole = $this->getUserRole($userId, $roleId);

        if ($userRole) {
            if (!$this->userRoleTable->delete($userRole)) {
                throw new InternalErrorException('Unable to delete user role');
            }
        }
    }

    public function addEnsembleToRole(int $ensembleId, int $roleId)
    {
        if (!$this->ensembleRoleExists($ensembleId, $roleId)) {
            $userRole = $this->ensembleRoleTable->newEntity();
            $userRole->ensembleId = $ensembleId;
            $userRole->roleId = $roleId;

            if (!$this->ensembleRoleTable->save($userRole)) {
                throw new InternalErrorException('Unable to save user role');
            }
        }
    }

    private function ensembleRoleExists(int $ensembleId, int $roleId)
    {
        return $this->getEnsembleRole($ensembleId, $roleId) != null;
    }

    private function getEnsembleRole(int $ensembleId, int $roleId)
    {
        $this->findById($roleId);
        $this->EnsembleService->findById($ensembleId);

        return $this->ensembleRoleTable->find()
            ->where([
                'ensembleId' => $ensembleId,
                'roleId' => $roleId,
            ])
            ->first();
    }

    public function removeEnsembleFromRole(int $ensembleId, int $roleId)
    {
        $ensembleRole = $this->getEnsembleRole($ensembleId, $roleId);

        if ($ensembleRole) {
            if (!$this->ensembleRoleTable->delete($ensembleRole)) {
                throw new InternalErrorException('Unable to delete ensemble role');
            }
        }
    }
}
