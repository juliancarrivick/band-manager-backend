<?php
namespace App\Controller;

use App\Controller\AppController;

class PerformanceController extends AppController {
    public $components = [
        'PerformanceService'
    ];

    public function index() {
        if (is_numeric($this->request->getParam('concert_id'))) {
            $concertId = $this->request->getParam('concert_id');
            $performance = $this->PerformanceService->findByConcertId($concertId);
        } else {
            $performance = $this->PerformanceService->findAll();
        }

        $this->set('performance', $performance);
        $this->set('_serialize', ['performance']);
    }

    public function view($id = null) {
        $performance = $this->PerformanceService->findById($id);

        $this->set('performance', $performance);
        $this->set('_serialize', ['performance']);
    }

    public function add() {
        $performance = $this->PerformanceService->create($this->request->getData());

        $this->set('performance', $performance);
        $this->set('_serialize', ['performance']);
    }

    public function edit($id = null) {
        $performance = $this->PerformanceService->update($id, $this->request->getData());

        $this->set('performance', $performance);
        $this->set('_serialize', ['performance']);
    }

    public function delete($id = null) {
        $this->PerformanceService->delete($id);
    }
}
