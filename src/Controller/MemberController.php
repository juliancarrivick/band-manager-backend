<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;

class MemberController extends AppController
{
    /* Automatically include the Member Service */
    public $components = [
        'MemberService',
        'SecurityService',
    ];

    public function isAuthorized($user)
    {
        return $this->AuthorisationService->userHasRole($user, 'admin') ||
        $this->AuthorisationService->userHasRole($user, 'editor');
    }

    public function index()
    {
        $members = $this->MemberService->findAll();

        $this->set('member', $members);
        $this->set('_serialize', ['member']);
    }

    public function view($id = null)
    {
        $member = $this->MemberService->findById($id);

        $this->set('member', $member);
        $this->set('_serialize', ['member']);
    }

    public function add()
    {
        $this->request->allowMethod(['post']);

        $member = $this->MemberService->create($this->request->getData());

        $this->set('member', $member);
        $this->set('_serialize', ['member']);
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        $member = $this->MemberService->update($id, $this->request->getData());

        $this->set('member', $member);
        $this->set('_serialize', ['member']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $this->MemberService->delete($id);
    }

    public function currentMembers()
    {
        $this->request->allowMethod(['get']);

        $currentMembers = $this->MemberService->currentMembers();

        $this->set('member', $currentMembers);
        $this->set('_serialize', ['member']);
    }

    public function invite()
    {
        $memberId = $this->request->getParam('member_id');

        if (!$memberId) {
            throw new BadRequestException('MemberId is required');
        }

        $this->SecurityService->inviteMemberById($memberId);
    }
}
