<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;

class AssetController extends AppController {
    /* Automatically include the Asset Service */
    public $components = [
      'AssetService'
    ];

    /**
     * Index method, returns all Assets.
     *
     * @return void
     */
    public function index()
    {
        $assets = $this->AssetService->findAll();

        $this->set('asset', $assets);
        $this->set('_serialize', ['asset']);
    }

    /**
     * View method
     *
     * @param string|null $id Asset id.
     * @return void
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asset = $this->AssetService->findById($id);

        $this->set('asset', $asset);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $asset = $this->AssetService->create($this->request->getData());

        $this->set('asset', $asset);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Asset id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        $asset = $this->AssetService->update($id, $this->request->getData());

        $this->set('asset', $asset);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $this->AssetService->delete($id);
    }
}
