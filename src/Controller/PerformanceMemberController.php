<?php
namespace App\Controller;

use App\Controller\AppController;

class PerformanceMemberController extends AppController {
    public $components = [
        'PerformanceMemberService'
    ];

    public function index() {
        $performanceId = $this->request->getParam('performance_id');
        $members = $this->PerformanceMemberService->findByPerformanceId($performanceId);

        $this->set('member', $members);
        $this->set('_serialize', ['member']);
    }

    public function add() {
        $memberId = $this->request->getParam('member_id');
        $performanceId = $this->request->getParam('performance_id');

        $this->PerformanceMemberService->addPerformanceMembership($performanceId, $memberId);
    }

    public function delete($id = null) {
        $memberId = $this->request->getParam('member_id');
        $performanceId = $this->request->getParam('performance_id');

        $this->PerformanceMemberService->deletePerformanceMembership($performanceId, $memberId);
    }
}
