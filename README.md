# Band Manager Backend

[![build status](https://gitlab.com/juliancarrivick/band-manager-backend/badges/master/build.svg)](https://gitlab.com/juliancarrivick/band-manager-backend/commits/master)
[![coverage report](https://gitlab.com/juliancarrivick/band-manager-backend/badges/master/coverage.svg)](https://gitlab.com/juliancarrivick/band-manager-backend/commits/master)

This project aims to provide a management system for bands to use for asset,
member, ensemble and performance records. It was born out of the desire for the
[Leeming Area Community Bands](http://www.lacb.org.au/) to have a proper asset
register and has since evolved into a full on management system to replace the
spreadsheet that used to be in use.

This part of the project contains the back end of the Band Manager. When
deployed to a PHP Server this  project provides a REST interface for the
[frontend](https://gitlab.com/juliancarrivick/band-manager-frontend) to
consume.

Generated using [CakePHP](http://cakephp.org) 3.x.

## Install Development Tools

- Install PHP 7: `apt install php7.2`
- Download [Composer](http://getcomposer.org/doc/00-intro.md) or update
`composer self-update`.
- You will then need to install and enable the `intl`, `sqlite` and `mysql` extensions
- Install MariaDB

## Dependencies

To install all dependencies execute `composer install`

## Configuration

Make a copy of `config/app.default.php` and call it `config/app.php`
and setup the 'Datasources' configuration data.

## Serving

To serve a local copy of the backend run `bin/cake server`, the service
will be exposed at `http://localhost:8765/`.

## Testing

To run all unit and integration tests execute `vendor/bin/phpunit`

To calculate code coverage you must install and enable xdebug
(`apt install php5-xdebug && php5enmod xdebug`) then
run `vendor/bin/phpunit --coverage-html webroot/coverage tests/`.

## Deployment

Ensure that `'debug' = false` is set in `config/app.php` and that the
default Datasource is set to your production database.
Set the `'Security' => 'salt'` value in `config/app.php`.
