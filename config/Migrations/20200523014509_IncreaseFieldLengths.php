<?php

use Migrations\AbstractMigration;

class IncreaseFieldLengths extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('score')
            ->changeColumn('title', 'string', [
                'null' => false,
                'limit' => 90,
                'default' => null
            ])
            ->changeColumn('composer', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->changeColumn('arranger', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->changeColumn('grade', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->changeColumn('location', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->changeColumn('genre', 'string', [
                'null' => true,
                'limit' => 90,
                'default' => null,
            ])
            ->addColumn('inLibrary', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->save();
        $this->table('users')
            ->changeColumn('username', 'string', [
                'limit' => 90,
            ])
            ->save();
    }
}
